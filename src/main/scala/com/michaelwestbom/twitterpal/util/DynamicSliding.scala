package com.michaelwestbom.twitterpal.util

import akka.stream.{FlowShape, Inlet, Outlet}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}

import scala.collection.immutable

final case class DynamicSliding[T](n: Int, step: Int = 1) extends GraphStage[FlowShape[T, immutable.Seq[T]]] {
  require(n > 0, "n must be greater than 0")
  require(step > 0, "step must be greater than 0")

  val in: Inlet[T] = Inlet[T]("DynamicSliding.in")
  val out: Outlet[immutable.Seq[T]] = Outlet[immutable.Seq[T]]("DynamicSliding.out")

  override def shape: FlowShape[T, immutable.Seq[T]] = FlowShape(in, out)

  // Stole most of this from here: https://github.com/akka/akka/blob/3307b3522cdc559643444f4e4fe0d8b3807cdeb6/akka-stream/src/main/scala/akka/stream/impl/fusing/Ops.scala#L796-L849
  override def createLogic(inheritedAttributes: _root_.akka.stream.Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {
      private var buf = Vector.empty[T]

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          buf :+= grab(in)
          if (buf.size <= n) {
            push(out, buf)
          } else if (step <= n) {
            buf = buf.drop(step)
            if (buf.size == n) {
              push(out, buf)
            } else {
              pull(in)
            }
          } else if (step > n) {
            if (buf.size == step) {
              buf = buf.drop(step)
            }
            pull(in)
          }
        }
      })

      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
          pull(in)
        }
      })
    }
}
