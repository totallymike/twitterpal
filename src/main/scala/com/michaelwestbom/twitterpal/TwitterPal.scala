package com.michaelwestbom.twitterpal

import java.time.Instant

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpRequest, Uri}
import akka.http.scaladsl.{Http, HttpExt}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{JsonFraming, Sink}
import cats.implicits._
import com.michaelwestbom.twitterpal.twitter.auth.{OauthClient, OauthHeader}
import com.michaelwestbom.twitterpal.twitter.stream.{MetricsCalculator, parseTweets}
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Random, Success}

object TwitterPal extends App {
  private implicit val system: ActorSystem = ActorSystem()
  private implicit val ec: ExecutionContext = system.dispatcher
  private implicit val materializer: ActorMaterializer = ActorMaterializer()

  val rand: Random = new Random()
  val config: Config = ConfigFactory.load()
  val oauthClient: OauthClient = OauthClient.fromConfig(config)
  val url: Uri = Uri("https://stream.twitter.com/1.1/statuses/sample.json")
  var req: HttpRequest = HttpRequest(uri = url)

  val oauthParams: OauthHeader = OauthHeader(
    request = req,
    nonce = rand.alphanumeric.take(24).toString,
    timestamp = Instant.now,
    oauthClient = oauthClient
  )

  req = req.withHeaders(List(oauthParams))
  val http: HttpExt = Http()

  val esc = 27.toChar
  val clearScreen = "" + esc + "[2J" + esc + "[1;1H"
  val processor: Future[Done] = for {
    response <- http.singleRequest(req)
    body <- response.entity
      .withoutSizeLimit()
      .dataBytes
      .via(JsonFraming.objectScanner(95916))
      .via(parseTweets)
      .async
      .via(MetricsCalculator.apply)
        .runWith(Sink.foreach(metrics => {
          print(clearScreen)
          println(metrics.show)
        }))
  } yield body

  (for {
    _ <- processor
    _ <- http.shutdownAllConnectionPools()
    terminated <- system.terminate()
  } yield terminated).onComplete({
    case Success(value) => println(s"Done: $value")
    case Failure(error) => println(s"ERROR: ${error.getMessage}")
  })
}
