package com.michaelwestbom.twitterpal.twitter.auth

import cats.Eval
import cats.data.{State, StateT}
import com.typesafe.config.Config

final case class ConsumerKey(value: String) extends AnyVal
final case class ConsumerSecret(value: String) extends AnyVal
final case class Consumer(key: ConsumerKey, secret: ConsumerSecret)

final case class AccessKey(value: String) extends AnyVal
final case class AccessSecret(value: String) extends AnyVal
final case class Credentials(key: AccessKey, secret: AccessSecret)

final case class OauthClient(consumer: Consumer, credentials: Credentials)

object OauthClient {
  def fromConfig(config: Config): OauthClient = {
    val loadData = for {
      consumer <- loadConsumer
      credentials <- loadCredentials
    } yield OauthClient(consumer, credentials)
    loadData.run(config).value._2
  }

  def loadConsumer: StateT[Eval, Config, Consumer] =
    for {
      key <- loadConsumerKey
      secret <- loadConsumerSecret
    } yield Consumer(key, secret)

  def loadCredentials: State[Config, Credentials] =
    for {
      key <- loadAccessKey
      secret <- loadAccessSecret
    } yield Credentials(key, secret)

  val loadConsumerKey: State[Config, ConsumerKey] = State(config =>
    (config,
     ConsumerKey(config.getString("twitter.consumer_key"))))

  val loadConsumerSecret: State[Config, ConsumerSecret] =
    State(config =>
      (config,
       ConsumerSecret(config.getString("twitter.consumer_secret"))))

  val loadAccessKey: State[Config, AccessKey] =
    State(config =>
      (config,
       AccessKey(config.getString("twitter.access_key"))))

  val loadAccessSecret: State[Config, AccessSecret] =
    State(config =>
      (config,
       AccessSecret(config.getString("twitter.access_secret"))))
}
