package com.michaelwestbom.twitterpal.twitter.auth

import java.time.Instant
import java.util.Base64
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.headers.CustomHeader

import scala.collection.SortedMap

object OauthHeader {
  private[auth] def urlEncode(s: String) =
    s.flatMap(char => {
      if (char.isLetterOrDigit || reservedChars.contains(char)) {
        s"$char"
      } else {
        f"%%${char.toByte}%2X"
      }

    })

  private val reservedChars: Array[Char] =
    Array('-', '.', '_', '~')
}
final case class OauthHeader(request: HttpRequest, bodyFields: Option[Map[String, String]] = None, nonce: String, timestamp: Instant, oauthClient: OauthClient) extends CustomHeader {
  private val mac = Mac.getInstance("HmacSHA1")

  override def value(): String = {
    s"""OAuth oauth_consumer_key="${urlEncode(consumerKey)}",""" +
      s"""oauth_nonce="${urlEncode(nonce)}",""" +
      s"""oauth_signature="${urlEncode(signature)}",""" +
      """oauth_signature_method="HMAC-SHA1",""" +
      s"""oauth_timestamp="${timestamp.getEpochSecond}",""" +
      s"""oauth_token="${urlEncode(accessKey)}",""" +
      """oauth_version="1.0""""
  }

  override def name(): String = "Authorization"

  override def renderInRequests(): Boolean = true

  override def renderInResponses(): Boolean = false

  def parameterString: String = {
    var parameters = SortedMap.empty[String, String]
    request.uri.query().foreach({
      case (k, v) =>
        parameters = parameters + (urlEncode(k) -> urlEncode(v))
    })
    bodyFields match {
      case Some(fields) =>
        fields.foreach({
          case (k, v) => {
            parameters = parameters + (urlEncode(k) -> urlEncode(v))
          }
        })
      case None => {}
    }

    parameters = parameters + (
      "oauth_consumer_key" -> urlEncode(consumerKey),
      "oauth_nonce" -> urlEncode(nonce),
      "oauth_signature_method" -> "HMAC-SHA1",
      "oauth_timestamp" -> s"${timestamp.getEpochSecond}",
      "oauth_token" -> urlEncode(accessKey),
      "oauth_version" -> "1.0"
    )
    parameters.map({
      case (key, value) => s"$key=$value"
    }).mkString("&")
  }

  private[auth] def baseString: String = {
    val methodString = request.method.value
    val bareUri = request.uri.withQuery(Query.Empty)
    s"${methodString}&${urlEncode(bareUri.toString)}&${urlEncode(parameterString)}"
  }

  private[auth] def signingKey: String =
    s"${urlEncode(consumerSecret)}&${urlEncode(accessSecret)}"

  def signature: String = {
    val secretKey = new SecretKeySpec(signingKey.getBytes(), "HmacSHA1")
    mac.init(secretKey)
    val signature = mac.doFinal(baseString.getBytes)
    Base64.getEncoder.encodeToString(signature)
  }

  private[auth] def urlEncode(s: String) =
    OauthHeader.urlEncode(s)

  private def accessKey: String = oauthClient.credentials.key.value
  private def accessSecret: String = oauthClient.credentials.secret.value
  private def consumerKey: String = oauthClient.consumer.key.value
  private def consumerSecret: String = oauthClient.consumer.secret.value
}
