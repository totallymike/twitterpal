package com.michaelwestbom.twitterpal.twitter.auth

import java.time.Instant

import akka.http.scaladsl.model.HttpRequest

import scala.util.Random

/**
  * I had some idea of allowing for generation of a partial
  * config but it's not really worth it.
  * I should remove this eventually.
  */
final case class PartialOauthHeader(request: HttpRequest,
                                    nonce: Option[String] = None,
                                    timestamp: Option[Instant] = None) {
  def to_complete(oauthClient: OauthClient, rand: Random, bodyFields: Option[Map[String, String]] = None): OauthHeader = {
    OauthHeader(request = request, bodyFields = bodyFields, nonce = nonce.getOrElse(rand.alphanumeric.take(24).toString()), timestamp = timestamp.getOrElse(Instant.now), oauthClient = oauthClient)
  }
}
