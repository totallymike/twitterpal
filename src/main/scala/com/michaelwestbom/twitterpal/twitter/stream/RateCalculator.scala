package com.michaelwestbom.twitterpal.twitter.stream

import akka.NotUsed
import akka.stream.scaladsl.{Flow, GraphDSL, Source}
import GraphDSL.Implicits._
import akka.stream.stage._
import akka.stream._
import com.michaelwestbom.twitterpal.twitter.model.Tweet
import com.michaelwestbom.twitterpal.util.DynamicSliding

import scala.concurrent.duration.{FiniteDuration, HOURS, MINUTES, SECONDS, TimeUnit}

/**
  * Keep a moving average of how many tweets one gets per a given duration.
  */
case class TimedCounter[T](length: Int, unit: TimeUnit) extends GraphStage[FlowShape[T, Long]] {
  val in: Inlet[T] = Inlet[T]("TimedCounter.in")
  val out: Outlet[Long] = Outlet[Long]("TimedCounter.out")

  private val durationMap: Map[TimeUnit, FiniteDuration] =
    Map(SECONDS -> FiniteDuration(1, SECONDS),
      MINUTES -> FiniteDuration(15, SECONDS),
      HOURS -> FiniteDuration(1, MINUTES))
  private val multiplierMap: Map[TimeUnit, Long] =
    Map(SECONDS -> 1L,
      MINUTES -> 4L,
      HOURS -> 60L)

  override def shape: FlowShape[T, Long] =
    FlowShape.of(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new TimerGraphStageLogic(shape) {
      private var currentPeriodCount: Long = 0L
      private var hasStarted: Boolean = false
      private val duration = durationMap(unit)
      private val multiplier = multiplierMap(unit)

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          currentPeriodCount += 1
          if (!hasStarted) {
            schedulePeriodically(None, duration)
            hasStarted = true
          }
          pull(in)
        }
      })

      override protected def onTimer(timerKey: Any): Unit = {
        val periodCount = currentPeriodCount
        currentPeriodCount = 0
        emit(out, periodCount * multiplier)
      }

      setHandler(out, new OutHandler {
        override def onPull(): Unit =
          if (!hasBeenPulled(in)) pull(in)

      })
    }
}

object RateCalculator {
  /**
    * @param length
    * @param unit
    * @return a flow that generates an updated average every 1 `unit`
    */
  def apply(length: Int, unit: TimeUnit): Graph[FlowShape[Tweet, Long], NotUsed] =
    GraphDSL.create() { implicit builder =>
      val duration = FiniteDuration(1, unit)
      val oneSecond = FiniteDuration(1, SECONDS)
      val multiplier = duration / oneSecond

      val historyLength = length * multiplier.round.toInt
      val Tweets = builder.add(TimedCounter[Tweet](historyLength, unit))
      val average = builder.add(MovingAverage(historyLength, name = unit.name()))

      /* Since this stage is time based, and only emits periodically,
       * we want to be able to provide the most recent update whenever asked for.
       * That said, this may not be fantastic if we needed to track this over time, rather
       * than provide a moving average.
       */
      val repeatUntilRefresh = builder.add(
        Flow[Long]
          .prepend(Source.single(0L))
          .expand(n => Iterator.continually(n))
      )

      Tweets.out ~> average ~> repeatUntilRefresh
      FlowShape(Tweets.in, repeatUntilRefresh.out)
    }

  def Counter(): Flow[Tweet, Int, NotUsed] =
    Flow[Tweet]
      .detach
      .conflateWithSeed(_ => 1)({
        case (acc, _) => acc + 1
      })

  def MovingAverage(length: Int, name: String = "MovingAverage"): Flow[Long, Long, NotUsed] =
    Flow[Long]
      .via(DynamicSliding(length))
      .log(name)
      .map(counts => {
        counts.sum / counts.length
      })
}

