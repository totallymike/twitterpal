package com.michaelwestbom.twitterpal.twitter

import akka.NotUsed
import akka.stream.scaladsl.Flow
import akka.util.ByteString
import io.circe.parser.decode
import com.michaelwestbom.twitterpal.twitter.model._
import com.michaelwestbom.twitterpal.twitter.stream.Codec._

package object stream {
  val parseTweets: Flow[ByteString, Tweet, NotUsed] =
    Flow[ByteString]
      .map(_.utf8String)
      .map(decode[Tweet])
      .collect {
        case Right(tweet) => tweet
      }

  val TweetCounter: Flow[Tweet, Int, NotUsed] =
    Flow[Tweet]
      .scan[Int](0)(
      (total, _) => total + 1)

}
