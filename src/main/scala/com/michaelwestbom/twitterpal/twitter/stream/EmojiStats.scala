package com.michaelwestbom.twitterpal.twitter.stream

import akka.NotUsed
import akka.stream.scaladsl.{Balance, Flow, GraphDSL, Merge}
import GraphDSL.Implicits._
import akka.stream.FlowShape
import cats.Show
import cats.implicits._
import com.michaelwestbom.twitterpal.twitter.model._

object EmojiStats {
  val EmojiCounter: Flow[Tweet, TopEmoji, NotUsed] =
    Flow.fromGraph(GraphDSL.create() { implicit builder =>
      val dispatchTweets = builder.add(Balance[Tweet](2))
      val mergeEmoji = builder.add(Merge[(Boolean, Seq[Emoji])](2))
      val counter = builder.add(countEmoji)

      dispatchTweets.out(0) ~> findEmoji.async ~> mergeEmoji.in(0)
      dispatchTweets.out(1) ~> findEmoji.async ~> mergeEmoji.in(1)
      mergeEmoji ~> counter
      FlowShape(dispatchTweets.in, counter.out)
    })

  private def findEmoji: Flow[Tweet, (Boolean, Seq[Emoji]), NotUsed] =
    Flow[Tweet]
      .map(tweet => {
        val emojiList = Emoji.findAllInString(tweet.text)
        (emojiList.nonEmpty, emojiList)
      })
      .log("findEmoji")

  private def countEmoji: Flow[(Boolean, Seq[Emoji]), TopEmoji, NotUsed] =
    Flow[(Boolean, Seq[Emoji])]
      .log("count emoji")
      .scan[TopEmoji](TopEmoji.empty)((topEmoji, newData) => {
        val (hasEmoji, emojiList) = newData
        val updatedTopEmoji =
          (emojiList foldLeft topEmoji)((topEmoji, emoji) => {
            topEmoji.increment(emoji)
          })
        if (hasEmoji) {
          updatedTopEmoji.incrementTotal
        } else {
          updatedTopEmoji
        }
    })
      .conflate[TopEmoji]({ case (_, topEmoji) => topEmoji})
}

final case class TopEmoji(tweetsWithEmoji: Long = 0L, override protected val internalMap: collection.immutable.Map[Emoji, Long] = Map.empty.withDefaultValue(0L))
  extends AbstractMapBackedRankedEntities[Emoji] {

  def incrementTotal: TopEmoji =
    copy(tweetsWithEmoji = tweetsWithEmoji + 1, internalMap = this.internalMap)

  override def increment(key: Emoji): TopEmoji =
    copy(tweetsWithEmoji = this.tweetsWithEmoji, internalMap = internalMap.updated(key, internalMap(key) + 1))

  protected def copy(tweetsWithEmoji: Long, internalMap: collection.immutable.Map[Emoji, Long]): TopEmoji =
    TopEmoji(tweetsWithEmoji = tweetsWithEmoji, internalMap = internalMap)

  override protected def copy(internalMap: collection.immutable.Map[Emoji, Long]): TopEmoji =
    copy(tweetsWithEmoji = this.tweetsWithEmoji, internalMap = internalMap)
}

object TopEmoji extends ShowableRankedEntities {
  def empty: TopEmoji = TopEmoji()

  implicit val ordering: Ordering[EntityCount[Emoji]] =
    rankOrdering[Emoji]
  implicit val showTopEmoji: Show[TopEmoji] =
    Show.catsContravariantForShow
      .contramap(showRankedEntities[Emoji])(_.asInstanceOf[RankedEntities[Emoji]])
}
