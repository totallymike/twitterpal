package com.michaelwestbom.twitterpal.twitter.stream

import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import com.michaelwestbom.twitterpal.twitter.model.Tweet

class TweetCounter extends GraphStage[FlowShape[Tweet, Int]] {
  private val in = Inlet[Tweet]("TweetCounter.in")
  private val out = Outlet[Int]("TweetCounter.out")

  override def shape: FlowShape[Tweet, Int] =
    FlowShape.of(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {
      private var counter = 1

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          push(out, counter)
          counter += 1
        }
      })

      setHandler(out, new OutHandler {
        override def onPull(): Unit = pull(in)
      })
    }
}
