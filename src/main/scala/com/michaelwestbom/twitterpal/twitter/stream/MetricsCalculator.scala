package com.michaelwestbom.twitterpal.twitter.stream

import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Source, ZipWith}
import GraphDSL.Implicits._
import akka.NotUsed
import akka.stream.{FlowShape, Graph}
import cats.implicits._
import cats.Show
import com.michaelwestbom.twitterpal.twitter.model.{HashTag, RankedEntities, Tweet}
import EmojiStats.EmojiCounter

import scala.concurrent.duration.{Duration, HOURS, MINUTES, SECONDS}

case class Metrics(totalTweets: Int,
                   percentWithEmoji: Long,
                   percentWithUrls: Long,
                   percentWithPhotoUrls: Long,
                   percentWithMedia: Long,
                   topEmoji: TopEmoji,
                   topHashTags: RankedEntities[HashTag],
                   topUrls: TopUrls,
                   tweetsPerSecond: Long,
                   tweetsPerMinute: Long,
                   tweetsPerHour: Long)
object Metrics {
  implicit val showMetrics: Show[Metrics] = Show.show(metrics => {
    s"""--------------------------------
       |Tweets:
       |  total:            ${metrics.totalTweets}
       |  with emoji:       ${metrics.topEmoji.tweetsWithEmoji} (${metrics.percentWithEmoji}%)
       |  with links:       ${metrics.topUrls.tweetsWithUrls} (${metrics.percentWithUrls}%)
       |  with photo links: ${metrics.topUrls.tweetsWithPhotoUrls} (${metrics.percentWithPhotoUrls}%)
       |  with media:       ${metrics.topUrls.tweetsWithMedia} (${metrics.percentWithMedia}%)
       |
       |  per second: ${metrics.tweetsPerSecond}
       |  per minute: ${metrics.tweetsPerMinute}
       |  per hour:   ${metrics.tweetsPerHour}
       |--------------------------------
       |Top 5 Emoji:
       |${metrics.topEmoji.show}
       |--------------------------------
       |Top HashTags:
       |${metrics.topHashTags.show}
       |--------------------------------
       |Top Urls:
       |${metrics.topUrls.show}
       |--------------------------------
    """.stripMargin

  })
}

object MetricsCalculator {
  def apply: Graph[FlowShape[Tweet, Metrics], NotUsed] = GraphDSL.create() { implicit builder =>
    val duration = Duration(1, SECONDS)
    val B = builder.add(Broadcast[Tweet](7))

    val Ticker = builder.add(Source.tick(duration, duration, NotUsed))
    val hashTagCounter = builder.add(HashTagStats.hashTagStats())
    val emojiCounter = builder.add(EmojiCounter)
    val urlCounter = builder.add(UrlStats.UrlCounter)

    val Z = builder.add(ZipWith[Int, Long, Long, Long, TopEmoji, RankedEntities[HashTag], TopUrls, NotUsed, Metrics]({
      case (totalTweets, tps, tpm, tph, topEmoji, topHashTags, topUrls, _) => {
        // Should extract this duplication...
        val percentWithEmoji = topEmoji.tweetsWithEmoji match {
          case 0 => 0
          case n => (n.toDouble / totalTweets)
            .*(100)
            .round
        }
        val percentWithUrls = topUrls.tweetsWithUrls match {
          case 0 => 0
          case n => (n.toDouble / totalTweets)
              .*(100)
              .round
        }

        val percentWithPhotoUrls =
          topUrls.tweetsWithPhotoUrls match {
            case 0 => 0
            case n => (n.toDouble / totalTweets)
                .*(100)
                .round
          }

        val percentWithMedia = topUrls.tweetsWithMedia match {
          case 0 => 0
          case n => (n.toDouble / totalTweets)
              .*(100)
              .round
        }

        Metrics(
          totalTweets,
          percentWithEmoji,
          percentWithUrls,
          percentWithPhotoUrls,
          percentWithMedia,
          topEmoji,
          topHashTags,
          topUrls,
          tps, tpm, tph)
      }
    }))

    // @formatter:off
    B ~> TweetCounter ~> Flow[Int].conflate((_, latest) => latest) ~> Z.in0
    B ~> RateCalculator(60, SECONDS) ~> Z.in1
    B ~> RateCalculator(5, MINUTES)  ~> Z.in2
    B ~> RateCalculator(5, HOURS)    ~> Z.in3
    B ~> emojiCounter                ~> Z.in4
    B ~> hashTagCounter              ~> Z.in5
    B ~> urlCounter                  ~> Z.in6

    Ticker ~> Z.in7
    FlowShape(B.in, Z.out)
    // @formatter:on
  }
}
