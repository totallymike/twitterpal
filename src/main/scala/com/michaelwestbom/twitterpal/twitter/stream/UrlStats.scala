package com.michaelwestbom.twitterpal.twitter.stream

import akka.NotUsed
import akka.stream.scaladsl.{Balance, Flow, GraphDSL, Merge}
import GraphDSL.Implicits._
import akka.http.scaladsl.model.Uri.Path
import akka.stream.FlowShape
import cats.Show
import com.michaelwestbom.twitterpal.twitter.model._

object UrlStats {
  def UrlCounter: Flow[Tweet, TopUrls, NotUsed] = {
    Flow.fromGraph(GraphDSL.create() { implicit builder =>
      val split = builder.add(Balance[Tweet](2))
      val merge = builder.add(Merge[(List[Link], List[Media])](2))
      val collectStats = builder.add(collectUriStats)

      split.out(0) ~> collectData.async ~> merge.in(0)
      split.out(1) ~> collectData.async ~> merge.in(1)

                                           merge ~> collectStats

      FlowShape(split.in, collectStats.out)
    })
  }

  private val collectData = Flow[Tweet].map { tweet =>
    val uris = tweet.entities.urls
    val media: List[Media] =
      tweet.extendedEntities
        .map(_.media)
        .getOrElse(List.empty)

    (uris, media)
  }
  private val collectUriStats: Flow[(List[Link], List[Media]), TopUrls, NotUsed] =
    Flow[(List[Link], List[Media])]
      .scan[TopUrls](TopUrls())((topUrls, newData) => {
        val (links, media) = newData
        var updatedTopUrls =
          (links foldLeft topUrls)((newTopUrls, link) => {
            val domain = Domain(
              link.expanded_url.authority.host.address()
            )
            newTopUrls.increment(domain)
          })
        if (links.nonEmpty) {
          updatedTopUrls = updatedTopUrls.incrementTotal
        }
        if (hasPictureUris(links)) {
          updatedTopUrls = updatedTopUrls.incrementTweetsWithPhotos
        }
        if (media.nonEmpty) {
          updatedTopUrls = updatedTopUrls.incrementTweetsWithMedia
        }
        updatedTopUrls
      })
      .conflate({
        case (_, newTopUrls) => newTopUrls
      })


  private def hasPictureUris(links: List[Link]): Boolean =
    links.exists(link => {
      val url = link.expanded_url
      url.authority.host.address match {
        case "pic.twitter.com" =>
          true
        case "www.instagram.com" =>
          // It's only really a link to an image
          // if it matches pattern /p/:id
          url.path.startsWith(Path("/p/"))
        case _ => false
      }
    })
}

case class TopUrls(tweetsWithUrls: Long = 0L, tweetsWithPhotoUrls: Long = 0L, tweetsWithMedia: Long = 0L, topDomains: RankedEntities[Domain] = RankedEntities.empty[Domain]) {
  def increment(domain: Domain): TopUrls =
    copy(topDomains = topDomains.increment(domain))

  def incrementTotal: TopUrls =
    copy(tweetsWithUrls = tweetsWithUrls + 1)

  def incrementTweetsWithPhotos: TopUrls =
    copy(tweetsWithPhotoUrls = tweetsWithPhotoUrls + 1)

  def incrementTweetsWithMedia: TopUrls =
    copy(tweetsWithMedia = tweetsWithMedia + 1)
}

object TopUrls {
  implicit val showTopUrls: Show[TopUrls] =
    Show.catsContravariantForShow
      .contramap(RankedEntities.showRankedEntities[Domain])(_.topDomains)
}
