package com.michaelwestbom.twitterpal.twitter.stream

import akka.NotUsed
import akka.stream.FlowShape
import akka.stream.scaladsl.{Balance, Flow, GraphDSL, Merge}
import GraphDSL.Implicits._
import com.michaelwestbom.twitterpal.twitter.model.{HashTag, RankedEntities, Tweet}

object HashTagStats {
  def hashTagStats(concurrency: Int = 2): Flow[Tweet, RankedEntities[HashTag], NotUsed] =
    Flow.fromGraph(GraphDSL.create() { implicit builder =>
      val dispatchTweets = builder.add(Balance[Tweet](concurrency))
      val mergeHashTags  = builder.add(Merge[HashTag](concurrency))
      val counter = builder.add(countHashTags)

      dispatchTweets.out(0) ~> findHashTags.async ~> mergeHashTags
      dispatchTweets.out(1) ~> findHashTags.async ~> mergeHashTags
      mergeHashTags.out ~> counter
      FlowShape(dispatchTweets.in, counter.out)
    })

  val findHashTags: Flow[Tweet, HashTag, NotUsed] =
    Flow[Tweet]
      .mapConcat(_.entities.hashtags)

  val countHashTags: Flow[HashTag, RankedEntities[HashTag], NotUsed] =
    Flow[HashTag]
        .scan(RankedEntities.empty[HashTag])({
          case (topHashTags, hashTag) =>
            topHashTags.increment(hashTag)
        })
      .conflate((_, newTopHashTags) => newTopHashTags)
}
