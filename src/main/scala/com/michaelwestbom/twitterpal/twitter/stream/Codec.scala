package com.michaelwestbom.twitterpal.twitter.stream

import akka.http.scaladsl.model.Uri
import cats.syntax.either._
import io.circe._
import io.circe.generic.semiauto._
import com.michaelwestbom.twitterpal.twitter.model._

object Codec {
  implicit val uriDecoder: Decoder[Uri] =
    Decoder.decodeString.emap { str =>
      Either.catchNonFatal(Uri(str))
        .leftMap(err => s"Uri error: ${err.getMessage}")
    }

  implicit val hashTagDecoder: Decoder[HashTag] =
    deriveDecoder[HashTag]

  implicit val linkDecoder: Decoder[Link] =
    deriveDecoder[Link]

  implicit val mediaTypeDecode: Decoder[MediaType] =
    Decoder.decodeString.emap[MediaType] {
      case "photo" => Photo.asRight
      case "video" => Video.asRight
      case "animated_gif" => AnimatedGif.asRight
      case _ => "Invalid Media Type".asLeft
    }

  implicit val mediaDecoder: Decoder[Media] =
    Decoder.forProduct1[MediaType, Media]("type")(Media.apply)

  implicit val tweetEntitiesDecoder: Decoder[TweetEntities] =
    deriveDecoder[TweetEntities]

  implicit val extendedEntitiesDecoder: Decoder[ExtendedEntities] =
    deriveDecoder[ExtendedEntities]

  implicit val tweetDecoder: Decoder[Tweet] =
    Decoder
      .forProduct3[String, TweetEntities, Option[ExtendedEntities], Tweet]("text", "entities", "extended_entities")(Tweet.apply)
}
