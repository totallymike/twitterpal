package com.michaelwestbom.twitterpal.twitter.model

import scala.collection.JavaConverters._
import cats.Show
import com.vdurmont.emoji
import com.vdurmont.emoji.EmojiManager

case class Emoji(unicodeBytes: String, private val javaRep: com.vdurmont.emoji.Emoji) {
  override def toString: String = unicodeBytes
  def shortName: Option[String] =
    javaRep.getAliases.asScala.headOption
}

object Emoji {
  def findAllInString(str: String): List[Emoji] = {
    EmojiParser.unicodeCandidates(str)
      .map(candidate => {
        val emoji = candidate.getEmoji
        val unicodeBytes = if (candidate.hasFitzpatrick) {
          emoji.getUnicode(candidate.getFitzpatrick)
        } else {
          emoji.getUnicode
        }
        Emoji(unicodeBytes, emoji)
      })
  }

  def fromString(str: String): Option[Emoji] = {
    if (EmojiManager.isEmoji(str)) {
      findAllInString(str).headOption
    } else {
      None
    }
  }

  // Rather than using `toString`, let's create a typeclass for Show
  implicit val showEmoji: Show[Emoji] = Show.show(_.unicodeBytes)

  implicit val emojiOrdering: Ordering[Emoji] =
    Ordering.by(_.shortName)
}

private object EmojiParser extends emoji.EmojiParser {
  def extractEmojis(str: String): Seq[String] =
    emoji.EmojiParser.extractEmojis(str).asScala

  def unicodeCandidates(str: String): List[emoji.EmojiParser.UnicodeCandidate] =
    emoji.EmojiParser.getUnicodeCandidates(str).asScala.toList
}
