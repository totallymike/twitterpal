package com.michaelwestbom.twitterpal.twitter.model

import akka.http.scaladsl.model.Uri
import cats.Show

final case class HashTag(text: String)

object HashTag {
  implicit val showHashTag: Show[HashTag] =
    Show.show(hashTag => s"#${hashTag.text}")

  implicit val orderHashTags: Ordering[HashTag] =
    Ordering.by(_.text)
}

sealed trait MediaType
case object Photo extends MediaType
case object Video extends MediaType
case object AnimatedGif extends MediaType

final case class Media(media_type: MediaType)

final case class Link(expanded_url: Uri)
final case class TweetEntities(hashtags: List[HashTag],
                               urls: List[Link])
final case class ExtendedEntities(media: List[Media])
sealed trait StreamData

final case class Tweet(text: String, entities: TweetEntities, extendedEntities: Option[ExtendedEntities])
  extends StreamData

// See the warnings and known issues here:
// https://circe.github.io/circe/codec.html
// for why this object is necessary
object StreamData
