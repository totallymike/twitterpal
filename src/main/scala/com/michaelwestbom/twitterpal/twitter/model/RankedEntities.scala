package com.michaelwestbom.twitterpal.twitter.model

import cats.implicits._
import cats.Show

import collection.immutable

final case class EntityCount[T](entity: T, count: Long)

object EntityCount {
  implicit def showEntityCount[T](implicit tShow: Show[T]): Show[EntityCount[T]] =
    Show.show(c => s"${c.entity.show} => ${c.count.show}")
}


trait RankedEntities[T]
  extends RankedEntitiesLike[T, RankedEntities[T]] {
}

trait RankedEntitiesLike[T, +This <: RankedEntitiesLike[T, This] with RankedEntities[T]] extends Iterable[EntityCount[T]] { self =>
  def increment(key: T): RankedEntities[T]
}

abstract class RankedEntitiesFactory[CC[T] <: RankedEntities[T] with RankedEntitiesLike[T, CC[T]]] {
  def empty[T]: CC[T]
}

trait MapBackingForRankedEntities[T] {
  this: RankedEntities[T] =>
  protected val internalMap: immutable.Map[T, Long]
}

trait AbstractMapBackedRankedEntities[T]
  extends RankedEntities[T]
    with MapBackingForRankedEntities[T] { self =>
  protected val internalMap: immutable.Map[T, Long]
  override def iterator: Iterator[EntityCount[T]] =
    internalMap
      .iterator
      .map((EntityCount.apply[T] _).tupled)

  override def increment(key: T): AbstractMapBackedRankedEntities[T] =
    copy(internalMap = internalMap.updated(key, internalMap(key) + 1))

  protected def copy(internalMap: immutable.Map[T, Long] = this.internalMap): AbstractMapBackedRankedEntities[T]
}

trait ShowableRankedEntities {
  implicit def rankOrdering[T](implicit tOrdering: Ordering[T]): Ordering[EntityCount[T]] =
    Ordering[(Long, T)]
      .on[EntityCount[T]](r => (r.count, r.entity))
      .reverse

  implicit def showRankedEntities[T](implicit tShow: Show[EntityCount[T]], tOrdering: Ordering[T]): Show[RankedEntities[T]] =
    Show.show(stats =>
      stats.toList.sorted
        .take(5)
        .map(_.show)
        .mkString("\n"))
}

object RankedEntities extends RankedEntitiesFactory[RankedEntities]
  with ShowableRankedEntities {
  def apply[T]: RankedEntities[T] = empty

  override def empty[T]: RankedEntities[T] =
    new ConcreteMapBackedRankedEntities[T]()

  class ConcreteMapBackedRankedEntities[T](protected [RankedEntities] val internalMap: immutable.Map[T, Long] = Map.empty[T, Long].withDefaultValue(0L))
    extends AbstractMapBackedRankedEntities[T] {
    override def copy(internalMap: Map[T, Long] = this.internalMap): ConcreteMapBackedRankedEntities[T] =
      new ConcreteMapBackedRankedEntities[T](internalMap)
  }
}

//trait RankedEntities[T] extends Iterable[EntityCount[T]] {
//  private val internalMap: immutable.Map[T, Long] = immutable.Map.empty[T, Long]
//  def copy(internalMap: immutable.Map[T, Long] = this.internalMap): RankedEntities[T]
//
//  def increment(entity: T): RankedEntities[T] =
//    copy(internalMap = internalMap.updated(entity, internalMap(entity) + 1))
//
//  override def iterator: Iterator[EntityCount[T]] =
//    internalMap
//      .iterator
//      .map((EntityCount.apply[T] _).tupled)
//}
//
//object RankedEntities {
//  def empty[T]: RankedEntities[T] =
//
//  private implicit def rankOrdering[T](implicit tOrdering: Ordering[T]): Ordering[EntityCount[T]] =
//    Ordering[(Long, T)]
//      .on[EntityCount[T]](r => (r.count, r.entity))
//      .reverse
//
//  implicit def showRankedEntities[T](implicit tShow: Show[EntityCount[T]], tOrdering: Ordering[T]): Show[RankedEntities[T]] =
//    Show.show(stats =>
//      stats.toList.sorted
//        .take(5)
//        .map(_.show)
//        .mkString("\n"))
//}
