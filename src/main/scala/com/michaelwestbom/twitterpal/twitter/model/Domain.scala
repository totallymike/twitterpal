package com.michaelwestbom.twitterpal.twitter.model

import akka.http.scaladsl.model.Uri
import cats.Show

import scala.util.matching.Regex

final case class Domain(domain: String)

object Domain {
  private val urlRegex: Regex = raw"https?://\S+".r

  implicit val showDomain: Show[Domain] =
    _.domain

  implicit val domainOrdering: Ordering[Domain] =
    Ordering.by(_.domain)

  def extractAllUris(message: String): List[Uri] = {
    urlRegex.findAllMatchIn(message).toList
      .map(domain => Uri(domain.matched))
  }
}