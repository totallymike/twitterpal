package com.michaelwestbom.twitterpal.util

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Keep
import akka.stream.testkit.scaladsl.{TestSink, TestSource}
import akka.testkit.TestKit
import org.scalatest.{BeforeAndAfterAll, FunSpecLike, Matchers}

class DynamicSlidingTest extends TestKit(ActorSystem("test-system"))
  with FunSpecLike
  with Matchers
  with BeforeAndAfterAll {
  implicit val mat = ActorMaterializer()

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
    super.afterAll()
  }

  describe("DynamicSliding") {
    it("behaves like a sliding stage, but emits even when not full") {
      val (pub, sub) = TestSource.probe[Int]
        .via(DynamicSliding(5))
        .toMat(TestSink.probe[Seq[Int]])(Keep.both)
        .run()

      pub
        .sendNext(1)
        .sendNext(2)
        .sendNext(3)
        .sendNext(4)
        .sendNext(5)
        .sendNext(6)
      sub
        .request(6)
        .expectNext(Seq(1))
        .expectNext(Seq(1,2))
        .expectNext(Seq(1,2,3))
        .expectNext(Seq(1,2,3,4))
        .expectNext(Seq(1,2,3,4,5))
        .expectNext(Seq(2,3,4,5,6))
    }
  }
}
