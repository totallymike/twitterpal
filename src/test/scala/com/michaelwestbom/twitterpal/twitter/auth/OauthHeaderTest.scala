package com.michaelwestbom.twitterpal.twitter.auth

import java.time.Instant

import akka.http.scaladsl.model.{HttpMethods, HttpRequest, Uri}
import com.typesafe.config.ConfigFactory
import org.scalatest._

import scala.util.Random

class OauthHeaderTest extends FunSpec
  with Matchers {
  // The example data for this suite is mostly stolen from
  // https://dev.twitter.com/oauth/overview/creating-signatures
  val rand = new Random()

  val uri = Uri("https://api.twitter.com/1.1/statuses/update.json")
    .withQuery(Uri.Query("include_entities" -> "true"))
  val access_key = "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb"
  val access_secret = "LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE"
  val consumer_key = "xvz1evFS4wEEPTGEFPHBog"
  val consumer_secret = "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw"

  val timestamp = Instant.ofEpochSecond(1318622958L)
  val nonce = "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg"

  val statusData = Map("status" -> "Hello Ladies + Gentlemen, a signed OAuth request!")

  val baseRequest = HttpRequest(method = HttpMethods.POST, uri = uri)

  private val config = ConfigFactory.parseString(
    s"""
      |twitter {
      |  consumer_key = "$consumer_key"
      |  consumer_secret = "$consumer_secret"
      |  access_key = "$access_key"
      |  access_secret = "$access_secret"
      |}
    """.stripMargin)
  val client = OauthClient.fromConfig(config)
  describe("OauthHeader") {
    it("urlencodes properly") {
      // See https://dev.twitter.com/oauth/overview/authorizing-requests
      val spacesAndStuff = "Hooray look stuff+things!"
      OauthHeader.urlEncode("tnnArxj06cWHq44gCs1OSKk/jLY=") shouldEqual "tnnArxj06cWHq44gCs1OSKk%2FjLY%3D"
      OauthHeader.urlEncode(spacesAndStuff) shouldEqual "Hooray%20look%20stuff%2Bthings%21"
    }
  }

  it("generates the authorization header value") {
    val oauthHeader = OauthHeader(baseRequest, bodyFields = Some(statusData), nonce = nonce, timestamp = timestamp, oauthClient = client)
    val expectedValue = """OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog",oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg",""" +
      """oauth_signature="hCtSmYh%2BiHYCEqBWrE7C7hYmtUk%3D",""" +
      """oauth_signature_method="HMAC-SHA1",""" +
      """oauth_timestamp="1318622958",""" +
      """oauth_token="370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb",""" +
      """oauth_version="1.0""""
    oauthHeader.value shouldEqual expectedValue
  }
  it("can create a parameter string") {
    val oauthHeader = OauthHeader(baseRequest, bodyFields = Some(statusData), nonce = nonce, timestamp = timestamp, oauthClient = client)
    oauthHeader.parameterString shouldEqual "include_entities=true&oauth_consumer_key=xvz1evFS4wEEPTGEFPHBog&oauth_nonce=kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1318622958&oauth_token=370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb&oauth_version=1.0&status=Hello%20Ladies%20%2B%20Gentlemen%2C%20a%20signed%20OAuth%20request%21"
    // oauthHeader.signature shouldEqual "tnnArxj06cWHq44gCs1OSKk/jLY="
  }

  it("creates the base string too") {
    val expectedBaseString = "POST&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fupdate.json&include_entities%3Dtrue%26oauth_consumer_key%3Dxvz1evFS4wEEPTGEFPHBog%26oauth_nonce%3DkYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1318622958%26oauth_token%3D370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb%26oauth_version%3D1.0%26status%3DHello%2520Ladies%2520%252B%2520Gentlemen%252C%2520a%2520signed%2520OAuth%2520request%2521"
    val oauthHeader = OauthHeader(baseRequest, bodyFields = Some(statusData), nonce = nonce, timestamp = timestamp, oauthClient = client)
    oauthHeader.baseString shouldEqual expectedBaseString
  }

  it("also makes the signing key") {
    val oauthHeader = OauthHeader(baseRequest, bodyFields = Some(statusData), nonce = nonce, timestamp = timestamp, oauthClient = client)
    val expectedSigningKey = "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw&LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE"
    oauthHeader.signingKey shouldEqual expectedSigningKey
  }

  it("generates signatures! Hooray!") {
    val oauthHeader = OauthHeader(baseRequest, bodyFields = Some(statusData), nonce = nonce, timestamp = timestamp, oauthClient = client)
    oauthHeader.signature shouldEqual "hCtSmYh+iHYCEqBWrE7C7hYmtUk="
  }
}
