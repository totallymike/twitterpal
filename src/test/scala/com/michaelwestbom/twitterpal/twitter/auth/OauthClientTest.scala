package com.michaelwestbom.twitterpal.twitter.auth

import com.typesafe.config.ConfigFactory
import org.scalatest.{FunSpec, Matchers}

class OauthClientTest extends FunSpec with Matchers {
  describe("Oauth for Twitter") {
    it("uses keys and stuff") {
      val config = ConfigFactory.parseString(
        """
          |twitter {
          |  consumer_key = foo_key
          |  consumer_secret = foo_secret
          |  access_key = bar_key
          |  access_secret = bar_secret
          |}
        """.stripMargin)
      val client = OauthClient.fromConfig(config)
      client.consumer shouldEqual Consumer(
        ConsumerKey("foo_key"),
        ConsumerSecret("foo_secret")
      )
      client.credentials shouldEqual Credentials(
        AccessKey("bar_key"),
        AccessSecret("bar_secret")
      )
    }

    it("behaves poorly if config values are missing") {
      val config = ConfigFactory.parseString(
        """
          |twitter {
          |  consumer_key = foo_key
          |  consumer_secret = foo_secret
          |  access_secret = bar_secret
          |}
        """.stripMargin)
      import com.typesafe.config.ConfigException

      assertThrows[ConfigException.Missing] {
        OauthClient.fromConfig(config)
      }
    }
  }
}
