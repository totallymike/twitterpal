package com.michaelwestbom.twitterpal.twitter.stream

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Keep
import akka.stream.testkit.scaladsl.{TestSink, TestSource}
import akka.testkit.TestKit
import org.scalatest.{BeforeAndAfterAll, FunSpecLike, Matchers}

 // import scala.concurrent.duration.SECONDS

class RateCalculatorTest extends TestKit(ActorSystem("TestSystem"))
  with FunSpecLike
  with Matchers
  with BeforeAndAfterAll {
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
    super.afterAll()
  }
  describe("RateCalculator") {
    // I broke this a bunch by seeding empty data.
    // Will fix when I rearrange how time-series data is stored
    it("keeps a rolling average of incoming rates") {
      val (pub, sub) = TestSource.probe[Long]
        .via(RateCalculator.MovingAverage(5))
        .detach
        .toMat(TestSink.probe[Long])(Keep.both)
        .run()

      // Fill it up and cycle the 100s
      (1 to 5).foreach(_ => pub.sendNext(100))
      sub
        .request(5)
        .expectNextN(Vector[Long](100, 100, 100, 100, 100))

      pub.sendNext(0)
      sub.request(1)
      sub.expectNext(80)

      sub.request(1)
      pub.sendNext(0)
      sub.expectNext(60)
      pub.sendComplete()
      sub.expectComplete()
    }
  }
}
