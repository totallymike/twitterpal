package com.michaelwestbom.twitterpal.twitter.model

import akka.http.scaladsl.model.Uri
import org.scalatest.{FunSpec, Matchers}

class DomainTest extends FunSpec with Matchers {
  describe("Domain") {
    it("can find domains in a string") {
      val domainString = "hey here's some urls https://t.co/abcdef1234?boo and https://t.co/fakesarefake"
      Domain.extractAllUris(domainString) shouldEqual List(Uri("https://t.co/abcdef1234?boo"), Uri("https://t.co/fakesarefake"))
    }
  }
}
