package com.michaelwestbom.twitterpal.twitter.model

import org.scalatest.{FunSpec, Matchers}

class EmojiTest extends FunSpec with Matchers {
  describe("Emoji") {
    it("can find emoji in a string") {
      val emojiString = "here's some emoji \uD83D\uDC76\uD83C\uDFFE ⛸🍎🐭"
      val babyMediumDarkSkinTone = Emoji.fromString("\uD83D\uDC76\uD83C\uDFFE")
      val skate = Emoji.fromString("⛸").get
      val apple = Emoji.fromString("🍎").get
      val mouse = Emoji.fromString("🐭").get
      Emoji.findAllInString(emojiString) shouldEqual List(babyMediumDarkSkinTone.get, skate, apple, mouse)
    }
  }
}
