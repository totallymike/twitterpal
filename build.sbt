import sbt.util

name := "twitterpal-scala"

version := "0.3"

scalaOrganization := "org.typelevel"
scalaVersion := "2.12.3-bin-typelevel-4"

inThisBuild(Seq(
  scalaOrganization := "org.typelevel",
  scalaVersion := "2.12.3-bin-typelevel-4"
))

scalacOptions ++= Seq(
  "-feature",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-unchecked",
  "-Ywarn-unused",
  "-Ypartial-unification",
  "-Ywarn-infer-any",
  "-Xlint",
  "-Xfuture",
  "-Xfatal-warnings",
  "-Xcheckinit",
  // These are for Typelevel Scala
  "-Yinduction-heuristics",
  "-Ykind-polymorphism",
  "-Xstrict-patmat-analysis",
  "-Yliteral-types",
)

scalacOptions in (Compile, console) ~= (_.filterNot(Set(
  "-Ywarn-unused:imports",
  "-Xfatal-warnings"
)))
autoAPIMappings := true

fork in Test := true

outputStrategy := Some(StdoutOutput)

val circeVersion = "0.8.0"

libraryDependencies ++= Seq(
  // Akka
  "com.typesafe.akka" %% "akka-actor" % "2.5.4" withSources() withJavadoc(),
  "com.typesafe.akka" %% "akka-stream" % "2.5.4" withSources() withJavadoc(),
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.4" % Test,
  "com.typesafe.akka" %% "akka-http" % "10.0.10" withSources() withJavadoc(),
  "com.typesafe.akka" %% "akka-http-testkit" % "10.0.10" % Test,

  // JSON
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,

  // Emoji
  "com.vdurmont" % "emoji-java" % "3.3.0",

  // Test
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % Test,

  // Config
  "com.typesafe" % "config" % "1.3.1",

  // Bring in a _bunch_ of junk _just_ for oauth
  "com.typesafe.play" %% "play-ahc-ws-standalone" % "1.0.1",

  // Utils
  "org.typelevel" %% "cats-core" % "1.0.0-MF",
  "com.chuusai" %% "shapeless" % "2.3.2",
)

enablePlugins(UniversalPlugin)
enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

packageName in Docker := "twitterpal"
dockerBaseImage := "openjdk:8u141-jdk"
dockerUsername := Some("totallymike")
dockerUpdateLatest := true
//wartremoverErrors in (Compile, compile) ++= Warts.allBut(
//  Wart.NonUnitStatements,
//  Wart.Var,
//  Wart.ImplicitConversion,
//  Wart.ImplicitParameter,
//  Wart.DefaultArguments,
//  Wart.Overloading,
//)
