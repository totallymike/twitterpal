addSbtPlugin("org.wartremover" % "sbt-wartremover" % "2.2.0")
addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "1.2.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.2.2")