# Twitterpal

## How to run

### Twitter API

You need Twitter API credentials.

The easiest way to get them is to review [these instructions](https://dev.twitter.com/oauth/overview/application-owner-access-tokens).

Create an application that will be twitterpal, name it `twitterpal-<my-twitter-username>`, then use the directions as described in the above page.

### Docker

```bash
export TWITTER_ACCESS_KEY=<twitter_access_key goes here>
export TWITTER_ACCESS_SECRET=<twitter_access_secret goes here>
export TWITTER_CONSUMER_KEY=<twitter_consumer_key goes here>
export TWITTER_CONSUMER_SECRET=<twitter_consumer_secret goes here>
docker run --rm totallymike/twitterpal:0.1
```

That's...pretty much it.

### docker-compose

I added a `docker-compose.yml`, but it's not of much use. Here's how, if you so choose:

#### Acquiring the image

If you don't have the image, you'll need to acquire it. Twitterpal uses [sbt-native-packager](https://github.com/sbt/sbt-native-packager) to build the docker image, and docker-compose isn't quite smart enough to do that.

Your choices are to either download the pre-built version, or build it locally:

#### Pulling the image

`docker pull totallymike/twitterpal:latest`

##### Building Locally

`sbt docker:publishLocal`

#### Configuration

Next you'll need to drop a .env file in the project directory. It needs to look like this:

```
TWITTER_ACCESS_KEY=abcdblahblah
TWITTER_ACCESS_SECRET=abcdblahblahsecret
TWITTER_CONSUMER_KEY=consumerblahblahkey
TWITTER_CONSUMER_SECRET=consumerblahblahsecret
```

Alternatively you can export the above variables in the shell session from which you'll be running `docker-compose`.

#### Running

From within the root project directory, run

```
docker-compose run --rm twitterpal
```

